'use strict';

let ckecked = document.getElementById('list');
ckecked.addEventListener('dblclick', function(e){
        e.target.classList.add("checked");
        toSave();
});

let list = document.querySelector('ul');
list.addEventListener('click', function(e){
    if(e.target.tagName === "SPAN"){
    let div = e.target.parentNode;
    div.remove();}
    toSave();
});

function newElement() {
    let li = document.createElement('li');
    let inputValue = document.getElementById('newEl').value;
    let r = document.createTextNode(inputValue);
    li.appendChild(r);
    if (inputValue == ""){
        alert("Введите ваше дело");
    }
    else {
        document.getElementById('list').appendChild(li);
    }
    document.getElementById('newEl').value = "";
    document.getElementById('newEl').focus();
    let span = document.createElement('SPAN');
    let text = document.createTextNode(" x");
    span.className = "close";
    span.appendChild(text);
    li.appendChild(span);
    toSave();
}

let savedInfo;
function toSave(){
    savedInfo = list.innerHTML;
    localStorage.setItem('savedInfo', savedInfo);
}

    if (localStorage.getItem('savedInfo')){
        list.innerHTML = localStorage.getItem('savedInfo')
    }